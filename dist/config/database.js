"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let connection = mongoose.createConnection(process.env.MONGODB_URI);
exports.connection = connection;
