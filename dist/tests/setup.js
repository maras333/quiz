"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
dotenv_1.config();
process.env.NODE_ENV = "test";
process.env.MONGODB_URI = "mongodb://localhost:27017/QuizzAppTest";
