"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const app_1 = require("../../app");
const chaiHttp = require("chai-http");
const seed_1 = require("../seed/seed");
const expect = chai.expect;
chai.use(chaiHttp);
beforeEach(seed_1.populateQuizes);
describe("Api Quiz", function () {
    it("should be able to get all quizes", (done) => {
        chai.request(app_1.server)
            .get("/api/quiz")
            .set("content-type", "application/json")
            .end((err, res) => {
            expect(res.statusCode).to.be.equal(200);
            expect(res.body).to.have.lengthOf(4);
            done();
        });
    });
    it("should be able to create quiz", (done) => {
        chai.request(app_1.server)
            .post("/api/quiz")
            .set("content-type", "application/json")
            .send({
            topic: "chemistry",
            name: "New chemistry quiz"
        })
            .end((err, res) => {
            expect(res.statusCode).to.be.equal(200);
            expect(res.body.topic).to.be.equal("chemistry");
            expect(res.body.name).to.be.equal("New chemistry quiz");
            done();
        });
    });
});
