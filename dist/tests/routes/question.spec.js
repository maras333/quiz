"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const app_1 = require("../../app");
const chaiHttp = require("chai-http");
const models_1 = require("../../models/models");
const seed_1 = require("../seed/seed");
const expect = chai.expect;
chai.use(chaiHttp);
beforeEach(seed_1.populateQuestions);
beforeEach(seed_1.populateQuizes);
describe("Api Question", function () {
    it("should be able to get all questions", (done) => {
        chai.request(app_1.server)
            .get("/api/question")
            .set("content-type", "application/json")
            .end((err, res) => {
            expect(res.statusCode).to.be.equal(200);
            expect(res.body).to.have.lengthOf(4);
            done();
        });
    });
    it("should be able to get question with it's answers", (done) => {
        const questionId = seed_1.questions[0]._id.toHexString();
        chai.request(app_1.server)
            .get(`/api/questionDetails/${questionId}`)
            .set("content-type", "application/json")
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            expect(res.body._id).to.be.equal(questionId);
            expect(res.body.answers).to.have.lengthOf(3);
            done();
        });
    });
    it("should be able to get question according to chosen topic", (done) => {
        const topic = 'Astronomy';
        chai.request(app_1.server)
            .get(`/api/questionByTopic/${topic}`)
            .set("content-type", "application/json")
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            expect(res.body).to.have.lengthOf(3);
            done();
        });
    });
    it("should be able to create question to specified quiz", (done) => {
        const quizId = seed_1.quizes[3]._id.toHexString();
        const newQuestion = "Is this a Sun?";
        chai.request(app_1.server)
            .post(`/api/question/${quizId}`)
            .set("content-type", "application/json")
            .send({
            body: newQuestion
        })
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            expect(res.body.body).to.be.equal(newQuestion);
            models_1.Question.find().then((questions) => {
                expect(questions).to.have.lengthOf(5);
                done();
            }).catch(e => done(e));
        });
    });
    it("should be able to delete question with it's answers", (done) => {
        const questionId = seed_1.questions[0]._id.toHexString();
        chai.request(app_1.server)
            .del(`/api/question/${questionId}`)
            .set("content-type", "application/json")
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            models_1.Question.find({ _id: questionId }).then((questions) => {
                expect(questions).to.have.lengthOf(0);
            }).catch(e => done(e));
            models_1.Answer.find({ question_id: questionId }).then((answers) => {
                expect(answers).to.have.lengthOf(0);
            }).catch(e => done(e));
            done();
        });
    });
    it("should give POSITIVE info if given topic has questions", (done) => {
        const topic = 'Astronomy';
        chai.request(app_1.server)
            .get(`/api/checkQuestionsExist/${topic}`)
            .set("content-type", "application/json")
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            expect(res.body.message).to.be.equal(`Questions for topic ${topic} exists`);
            done();
        });
    });
    it("should give NEGATIVE info if given topic has questions", (done) => {
        const topic = 'Random topic';
        chai.request(app_1.server)
            .get(`/api/checkQuestionsExist/${topic}`)
            .set("content-type", "application/json")
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            expect(res.body.message).to.be.equal(`Questions for topic ${topic} NOT exists`);
            done();
        });
    });
});
