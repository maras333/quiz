"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const app_1 = require("../../app");
const chaiHttp = require("chai-http");
const models_1 = require("../../models/models");
const seed_1 = require("../seed/seed");
const expect = chai.expect;
chai.use(chaiHttp);
beforeEach(seed_1.populateQuestions);
beforeEach(seed_1.populateAnswers);
describe("Api Answer", function () {
    it("should add answer to specyfic question", (done) => {
        const questionId = seed_1.questions[0]._id.toHexString();
        chai.request(app_1.server)
            .post(`/api/answer/${questionId}`)
            .set("content-type", "application/json")
            .send({
            body: "Answer nr 4",
            isTrue: true
        })
            .end((err, res) => {
            if (err) {
                return done(err);
            }
            expect(res.statusCode).to.be.equal(200);
            expect(res.body.body).to.be.equal("Answer nr 4");
            expect(res.body.isTrue).to.be.equal(true);
            models_1.Answer.find({ question_id: questionId }).then((questions) => {
                expect(questions).to.have.lengthOf(4);
            }).catch(e => done(e));
            done();
        });
    });
});
