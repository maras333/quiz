// import { Author, IAuthorDocument } from "../../models/models";
// import { populateAuthors, authors, populatePosts, posts } from "../seed/seed";
// import * as chai from "chai";
//
// const expect = chai.expect;
//
// beforeEach(populateAuthors);
// beforeEach(populatePosts);
//
// describe("Models Author", () => {
//
//   it("should insert new author", async () => {
//
//     const author = new Author();
//     author.name = "John";
//     author.age = 30;
//     author.description = "He is writer";
//
//     const res = await author.save();
//
//     expect(res).to.be.an("object");
//     expect(res.name).to.be.equal("John");
//   });
//
//   it("should update user", async () => {
//     const results: { nModified: number } = await Author.updateAuthor(authors[0]._id, "He is not writer");
//
//     expect(+results.nModified).to.be.equal(1);
//   });
//
//   it("should update by age", async () => {
//     const results: { nModified: number } = await Author.updateByAge(29, "Good one :)");
//     const author: IAuthorDocument = <IAuthorDocument>await Author.findById(authors[1]._id).lean().exec();
//     expect(author.description).to.be.equal("Good one :)");
//     expect(+results.nModified).to.be.equal(2);
//   });
// });
