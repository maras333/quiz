"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models/models");
const seed_1 = require("../seed/seed");
const chai = require("chai");
const expect = chai.expect;
beforeEach(seed_1.populateQuizes);
describe("Model Quiz", () => {
    it("should create new quiz", (done) => {
        const quiz = new models_1.Quiz();
        quiz.topic = "Tough";
        quiz.name = "New difficult quiz";
        quiz.save().then(res => {
            expect(res).to.be.an("object");
            expect(res.topic).to.be.equal("Tough");
            expect(res.name).to.be.equal("New difficult quiz");
        });
        done();
    });
});
