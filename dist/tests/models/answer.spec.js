"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models/models");
const seed_1 = require("../seed/seed");
const chai = require("chai");
const mongodb_1 = require("mongodb");
const expect = chai.expect;
beforeEach(seed_1.populateAnswers);
describe("Model Answer", () => {
    it("should create new answer", (done) => {
        const questionId = new mongodb_1.ObjectID();
        const answer = new models_1.Answer();
        answer.body = "Answer nr one";
        answer.question_id = questionId;
        answer.save().then(res => {
            expect(res).to.be.an("object");
            expect(res.body).to.be.equal("Answer nr one");
            expect(res.question_id).to.be.equal(questionId);
            done();
        }).catch(e => done(e));
    });
});
