"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models/models");
const seed_1 = require("../seed/seed");
const chai = require("chai");
const mongodb_1 = require("mongodb");
const expect = chai.expect;
beforeEach(seed_1.populateQuestions);
describe("Model Question", () => {
    it("should create new question", (done) => {
        const quizId = new mongodb_1.ObjectID();
        const question = new models_1.Question();
        question.body = "John";
        question.quiz_id = quizId;
        question.save().then(res => {
            expect(res).to.be.an("object");
            expect(res.body).to.be.equal("John");
            expect(res.quiz_id).to.be.equal(quizId);
            done();
        }).catch(e => done(e));
    });
    it("should find questions by quiz topic", (done) => {
        const topic = "Astronomy";
        models_1.Question.findByQuizTopic(topic).then(questions => {
            expect(questions).to.have.lengthOf(3);
            done();
        }).catch(e => done(e));
    });
    it("should create question to existing quiz", (done) => {
        const quizId = seed_1.quizes[3]._id.toHexString();
        const questionBody = { body: "New question?" };
        models_1.Question.createQuestionToQuiz(questionBody, quizId).then(question => {
            expect(question.body).to.be.equal("New question?");
            expect(question.quiz_id.toHexString()).to.equals(quizId);
            done();
        }).catch(e => done(e));
    });
    it("should delete question with existing answers", (done) => {
        const questionId = seed_1.questions[0]._id.toHexString();
        models_1.Question.deleteWithAnswers(questionId).then(res => {
            models_1.Question.find({ _id: questionId }).then(questions => {
                expect(questions).to.have.lengthOf(0);
            }).catch(e => done(e));
            models_1.Answer.find({ questionId: questionId }).then(answers => {
                expect(answers).to.have.lengthOf(0);
            }).catch(e => done(e));
            done();
        });
    });
    it("should return TRUE if topic has it's own questions", (done) => {
        const topic = "Astronomy";
        models_1.Question.checkQuestionsExistInTopic(topic).then(res => {
            expect(res).to.be.equal(true);
            done();
        }).catch(e => done(e));
    });
    it("should return FALSE if topic has NOT it's own questions", (done) => {
        const topic = "Geography";
        models_1.Question.checkQuestionsExistInTopic(topic).then(res => {
            expect(res).to.be.equal(false);
            done();
        }).catch(e => done(e));
    });
});
