"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const models_1 = require("../../models/models");
const quizOneId = new mongodb_1.ObjectID();
const quizTwoId = new mongodb_1.ObjectID();
const quizThreeId = new mongodb_1.ObjectID();
const quizFourId = new mongodb_1.ObjectID();
const questionOneId = new mongodb_1.ObjectID();
const questionTwoId = new mongodb_1.ObjectID();
const questionThreeId = new mongodb_1.ObjectID();
const questionFourId = new mongodb_1.ObjectID();
const answerOneId = new mongodb_1.ObjectID();
const answerTwoId = new mongodb_1.ObjectID();
const answerThreeId = new mongodb_1.ObjectID();
const answerFourId = new mongodb_1.ObjectID();
const quizes = [{
        _id: quizOneId,
        topic: 'astronomy',
        name: 'Quiz 1',
        create: Date.now()
    }, {
        _id: quizTwoId,
        topic: 'maths',
        name: 'Quiz 2',
        create: Date.now()
    }, {
        _id: quizThreeId,
        topic: 'astronomy',
        name: 'Quiz 3',
        create: Date.now()
    }, {
        _id: quizFourId,
        topic: 'biology with no q',
        name: 'Quiz 4',
        create: Date.now()
    }];
exports.quizes = quizes;
const questions = [{
        _id: questionOneId,
        body: 'Is this question number one about astronomy?',
        answers: [answerOneId, answerTwoId, answerThreeId],
        create: Date.now(),
        quiz_id: quizOneId
    }, {
        _id: questionTwoId,
        body: 'Is this question number two about astronomy?',
        answers: [answerFourId],
        create: Date.now(),
        quiz_id: quizThreeId
    }, {
        _id: questionThreeId,
        body: 'Is this question number three about astronomy?',
        answers: [],
        create: Date.now(),
        quiz_id: quizThreeId
    }, {
        _id: questionFourId,
        body: 'Is this question number four about maths?',
        answers: [],
        create: Date.now(),
        quiz_id: quizFourId
    }];
exports.questions = questions;
const answers = [{
        _id: answerOneId,
        body: 'Ans nr one - astronomy',
        answers: [],
        isTrue: false,
        question_id: questionOneId
    }, {
        _id: answerTwoId,
        body: 'Is this question number two about astronomy?',
        answers: [],
        isTrue: true,
        question_id: questionOneId
    }, {
        _id: answerThreeId,
        body: 'Is this question number three about astronomy?',
        answers: [],
        isTrue: false,
        question_id: questionOneId
    }, {
        _id: answerFourId,
        body: 'Is this question number four about maths?',
        answers: [],
        isTrue: true,
        question_id: questionTwoId
    }];
exports.answers = answers;
const populateQuizes = (done) => {
    models_1.Quiz.deleteMany({})
        .then(() => {
        return models_1.Quiz.insertMany(quizes);
    })
        .then(() => {
        done();
    });
};
exports.populateQuizes = populateQuizes;
const populateQuestions = (done) => {
    models_1.Question.deleteMany({})
        .then(() => {
        return models_1.Question.insertMany(questions);
    })
        .then(() => {
        done();
    });
};
exports.populateQuestions = populateQuestions;
const populateAnswers = (done) => {
    models_1.Answer.deleteMany({})
        .then(() => {
        return models_1.Answer.insertMany(answers);
    })
        .then(() => {
        done();
    });
};
exports.populateAnswers = populateAnswers;
