"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../../config/database");
const mongoose_1 = require("mongoose");
exports.AnswerSchema = new mongoose_1.Schema({
    body: {
        type: String,
        required: true
    },
    create: {
        type: Date,
        "default": Date.now
    },
    isTrue: {
        type: Boolean,
        default: false
    },
    question_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Question',
        required: true
    },
});
exports.Answer = database_1.connection.model("Answer", exports.AnswerSchema);
