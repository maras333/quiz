"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../../config/database");
const mongoose_1 = require("mongoose");
const QuizSchema = new mongoose_1.Schema({
    topic: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: [true, 'Type name of quiz!']
    },
    create: {
        type: Date,
        "default": Date.now
    }
});
exports.Quiz = database_1.connection.model("Quiz", QuizSchema);
