"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../../config/database");
const mongoose_1 = require("mongoose");
const model_1 = require("../quiz/model");
const model_2 = require("../answer/model");
const QuestionSchema = new mongoose_1.Schema({
    body: {
        type: String,
        required: true
    },
    create: {
        type: Date,
        default: Date.now
    },
    answers: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Answer'
        }],
    quiz_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Quiz',
        required: true
    },
});
QuestionSchema.static("findByQuizTopic", (topic) => __awaiter(this, void 0, void 0, function* () {
    try {
        let topicIds = yield model_1.Quiz.find({ topic: topic.toLowerCase() }).distinct('_id');
        return exports.Question
            .find({ quiz_id: { $in: topicIds } })
            .lean()
            .exec();
    }
    catch (e) {
        return { e };
    }
}));
QuestionSchema.static("createQuestionToQuiz", (questionBody, quizId) => __awaiter(this, void 0, void 0, function* () {
    try {
        const quiz = yield model_1.Quiz.findOne({ _id: quizId }).exec();
        if (!quiz) {
            return { success: false, status: 404, msg: `Quiz with id: ${quizId} NOT exists. Question not created` };
        }
        questionBody.quiz_id = quiz._id;
        const question = yield exports.Question.create(questionBody).catch(e => { return { errorMsg: e.message }; });
        return question;
    }
    catch (e) {
        return { success: false, status: 400, msg: `Question ${quizId} not created` };
    }
}));
QuestionSchema.static("deleteWithAnswers", (id) => __awaiter(this, void 0, void 0, function* () {
    try {
        let removedQuestion = yield exports.Question.findOneAndRemove({ _id: id }).exec();
        if (!removedQuestion) {
            return Promise.resolve({ success: false, status: 404, msg: `Question ${id} NOT exists` });
        }
        yield model_2.Answer.deleteMany({ question_id: id });
        return Promise.resolve({ success: true, status: 200, msg: `Question ${id} removed` });
    }
    catch (e) {
        return Promise.resolve({ success: true, status: 400, msg: `Question ${id} not removed` });
    }
}));
QuestionSchema.static("checkQuestionsExistInTopic", (topic) => __awaiter(this, void 0, void 0, function* () {
    try {
        let topicIds = yield model_1.Quiz.find({ topic: topic.toLowerCase() }).distinct('_id');
        let questionIds = yield exports.Question.find({ quiz_id: { $in: topicIds } }).select('_id');
        if (questionIds.length)
            return Promise.resolve(true);
        else
            return Promise.resolve(false);
    }
    catch (e) {
        return Promise.reject(e);
    }
}));
exports.Question = database_1.connection.model("Question", QuestionSchema);
