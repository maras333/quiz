"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("../models/answer/model");
const model_2 = require("../models/question/model");
exports.default = {
    getAllAnswers: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const answers = yield model_1.Answer.find({}).exec();
            response.status(200).json(answers);
        }
        catch (e) {
            response.status(400).json({ errorMsg: e });
        }
    }),
    createAnswerToQuestion: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            let answerData = request.body;
            const question = yield model_2.Question.findById(request.params.questionId).exec();
            if (!question) {
                response.status(404).json({ msg: `Question with id ${request.params.questionId} not found` });
                return;
            }
            answerData.question_id = question._id;
            const answer = yield model_1.Answer.create(answerData);
            yield model_2.Question.update({ _id: question._id }, { $push: { answers: answer._id } });
            response.status(200).json(answer);
        }
        catch (e) {
            response.status(400).json({ errorMsg: e });
        }
    })
};
