"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("../models/question/model");
exports.default = {
    getAllQuestions: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const questions = yield model_1.Question.find({}).exec();
            response.json(questions);
        }
        catch (e) {
            response.status(400).json(e);
        }
    }),
    getQuestionDetails: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const question = yield model_1.Question.findById(request.params.id).populate('answers').exec();
            if (!question)
                return response.json({ msg: `Question with id ${request.params.id} not found` });
            response.json(question);
        }
        catch (e) {
            response.status(400).json(e);
        }
    }),
    getQuestionByTopic: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const questions = yield model_1.Question.findByQuizTopic(request.params.topic);
            response.json(questions);
        }
        catch (e) {
            response.status(400).json({ msg: e });
        }
    }),
    createQuestionToQuiz: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const question = yield model_1.Question.createQuestionToQuiz(request.body, request.params.quizId);
            response.json(question);
        }
        catch (e) {
            response.status(400).json(e);
        }
    }),
    deleteQuestionWithAnswers: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const result = yield model_1.Question.deleteWithAnswers(request.params.id);
            if (result.status === 400) {
                response.status(400).json({ msg: result.msg });
                return;
            }
            else if (result.status === 404) {
                response.status(404).json({ msg: result.msg });
                return;
            }
            response.status(200).json({ msg: result.msg });
        }
        catch (e) {
            response.status(400).json({ errorMsg: e });
        }
    }),
    checkQuestionsExist: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const exists = yield model_1.Question.checkQuestionsExistInTopic(request.params.topic);
            if (exists) {
                response.status(200).json({ message: `Questions for topic ${request.params.topic} exists` });
            }
            else {
                response.status(200).json({ message: `Questions for topic ${request.params.topic} NOT exists` });
            }
        }
        catch (e) {
            response.status(400).json({ errorMsg: e });
        }
    })
};
