"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("../models/quiz/model");
exports.default = {
    getAllQuizes: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            const quizes = yield model_1.Quiz.find({}).exec();
            response.json(quizes);
        }
        catch (e) {
            response.status(400).json({ errorMsg: e });
        }
    }),
    createQuiz: (request, response) => __awaiter(this, void 0, void 0, function* () {
        try {
            request.body.topic = request.body.topic.toLowerCase();
            const quiz = yield model_1.Quiz.create(request.body);
            if (!quiz) {
                response.status(400).json({ msg: `Quiz not created` });
                return;
            }
            response.status(200).json(quiz);
        }
        catch (e) {
            response.status(400).json({ errorMsg: e });
        }
    })
};
