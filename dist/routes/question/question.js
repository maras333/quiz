"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const question_1 = require("../../controllers/question");
class QuestionRouter {
    static routes() {
        return express_1.Router()
            .get("/question", question_1.default.getAllQuestions)
            .get("/questionDetails/:id", question_1.default.getQuestionDetails)
            .get("/questionByTopic/:topic", question_1.default.getQuestionByTopic)
            .post("/question/:quizId", question_1.default.createQuestionToQuiz)
            .delete("/question/:id", question_1.default.deleteQuestionWithAnswers)
            .get("/checkQuestionsExist/:topic", question_1.default.checkQuestionsExist);
    }
}
exports.QuestionRouter = QuestionRouter;
