"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const quiz_1 = require("../../controllers/quiz");
class QuizRouter {
    static routes() {
        return express_1.Router()
            .get("/quiz", quiz_1.default.getAllQuizes)
            .post("/quiz", quiz_1.default.createQuiz);
    }
}
exports.QuizRouter = QuizRouter;
