"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const answer_1 = require("../../controllers/answer");
class AnswerRouter {
    static routes() {
        return express_1.Router()
            // get all answers
            .get("/answer", answer_1.default.getAllAnswers)
            // add answer to a specific question
            .post("/answer/:questionId", answer_1.default.createAnswerToQuestion);
    }
}
exports.AnswerRouter = AnswerRouter;
