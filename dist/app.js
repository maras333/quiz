"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const body_parser_1 = require("body-parser");
const path = require("path");
const question_1 = require("./routes/question/question");
const quiz_1 = require("./routes/quiz/quiz");
const answer_1 = require("./routes/answer/answer");
const swagger_1 = require("./routes/swagger");
const app = express();
const port = process.env.PORT;
app.use(body_parser_1.json());
app.use(body_parser_1.urlencoded({
    extended: true
}));
app.get("/", (_request, _response) => {
    _response.json({
        name: "Quiz management API"
    });
});
app.use("/api", answer_1.AnswerRouter.routes());
app.use("/api", question_1.QuestionRouter.routes());
app.use("/api", quiz_1.QuizRouter.routes());
app.use("/api/swagger", new swagger_1.APIDocsRouter().getRouter());
app.use("/docs", express.static(path.join(__dirname, './assets/swagger')));
app.use((err, _request, _response, _next) => {
    _response.status(err.status || 500);
    _response.json({
        error: "Server error"
    });
});
const server = app.listen(port, () => {
    console.log(`Started on port ${port}`);
});
exports.server = server;
