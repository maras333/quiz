import * as express from "express";
import { json, urlencoded } from "body-parser";
import * as http from "http";
import * as path from "path";

import { QuestionRouter } from "./routes/question/question";
import { QuizRouter } from "./routes/quiz/quiz";
import { AnswerRouter } from "./routes/answer/answer";
import { APIDocsRouter } from "./routes/swagger";

const app = express();
const port = process.env.PORT;

app.use(json());
app.use(urlencoded({
  extended: true
}));

app.get("/", (_request: express.Request, _response: express.Response) => {

  _response.json({
    name: "Quiz management API"
  })
});


app.use("/api", AnswerRouter.routes());
app.use("/api", QuestionRouter.routes());
app.use("/api", QuizRouter.routes());
app.use("/api/swagger", new APIDocsRouter().getRouter());
app.use("/docs", express.static(path.join(__dirname, './assets/swagger')));

app.use((err: Error & { status: number }, _request: express.Request, _response: express.Response, _next: express.NextFunction): void => {

    _response.status(err.status || 500);
    _response.json({
        error: "Server error"
    })
});
const server: http.Server = app.listen(port, () => {
    console.log(`Started on port ${port}`)
});

export { server };
