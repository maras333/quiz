import {ObjectID} from 'mongodb';
import { Quiz, Question, Answer } from "../../models/models";

const quizOneId = new ObjectID();
const quizTwoId = new ObjectID();
const quizThreeId = new ObjectID();
const quizFourId = new ObjectID();

const questionOneId = new ObjectID();
const questionTwoId = new ObjectID();
const questionThreeId = new ObjectID();
const questionFourId = new ObjectID();

const answerOneId = new ObjectID();
const answerTwoId = new ObjectID();
const answerThreeId = new ObjectID();
const answerFourId = new ObjectID();

const quizes = [{
  _id: quizOneId,
  topic: 'astronomy',
  name: 'Quiz 1',
  create: Date.now()
}, {
  _id: quizTwoId,
  topic: 'maths',
  name: 'Quiz 2',
  create: Date.now()
}, {
  _id: quizThreeId,
  topic: 'astronomy',
  name: 'Quiz 3',
  create: Date.now()
}, {
  _id: quizFourId,
  topic: 'biology with no q',
  name: 'Quiz 4',
  create: Date.now()
}];

const questions = [{
  _id: questionOneId,
  body: 'Is this question number one about astronomy?',
  answers: [answerOneId, answerTwoId, answerThreeId],
  create: Date.now(),
  quiz_id: quizOneId
}, {
  _id: questionTwoId,
  body: 'Is this question number two about astronomy?',
  answers: [answerFourId],
  create: Date.now(),
  quiz_id: quizThreeId
}, {
  _id: questionThreeId,
  body: 'Is this question number three about astronomy?',
  answers: [],
  create: Date.now(),
  quiz_id: quizThreeId
}, {
  _id: questionFourId,
  body: 'Is this question number four about maths?',
  answers: [],
  create: Date.now(),
  quiz_id: quizFourId
}];

const answers = [{
  _id: answerOneId,
  body: 'Ans nr one - astronomy',
  answers: [],
  isTrue: false,
  question_id: questionOneId
}, {
  _id: answerTwoId,
  body: 'Is this question number two about astronomy?',
  answers: [],
  isTrue: true,
  question_id: questionOneId
}, {
  _id: answerThreeId,
  body: 'Is this question number three about astronomy?',
  answers: [],
  isTrue: false,
  question_id: questionOneId
}, {
  _id: answerFourId,
  body: 'Is this question number four about maths?',
  answers: [],
  isTrue: true,
  question_id: questionTwoId
}];

const populateQuizes = (done: Function) => {
  Quiz.deleteMany({})
    .then(() => {
      return Quiz.insertMany(quizes);
    })
    .then(() => {
      done();
    });
}

const populateQuestions = (done: Function) => {
  Question.deleteMany({})
    .then(() => {
      return Question.insertMany(questions);
    })
    .then(() => {
      done();
    });
}

const populateAnswers = (done: Function) => {
  Answer.deleteMany({})
    .then(() => {
      return Answer.insertMany(answers);
    })
    .then(() => {
      done();
    });
}


export { quizes, populateQuizes, questions, populateQuestions, answers, populateAnswers }
