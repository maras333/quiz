import { Quiz, IQuizDocument } from "../../models/models";
import { populateQuizes } from "../seed/seed";
import * as chai from "chai";

const expect = chai.expect;

beforeEach(populateQuizes);

describe("Model Quiz", () => {

  it("should create new quiz", (done: Function) => {

    const quiz: IQuizDocument = new Quiz();
    quiz.topic = "Tough";
    quiz.name = "New difficult quiz";

    quiz.save().then(res => {
        expect(res).to.be.an("object");
        expect(res.topic).to.be.equal("Tough");
        expect(res.name).to.be.equal("New difficult quiz");
    });
    done();
  });

});
