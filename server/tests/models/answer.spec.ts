import { Answer, IAnswerDocument } from "../../models/models";
import { populateAnswers } from "../seed/seed";
import * as chai from "chai";
import { ObjectID } from "mongodb";

const expect = chai.expect;

beforeEach(populateAnswers);

describe("Model Answer", () => {

    it("should create new answer", (done: Function) => {
      const questionId = new ObjectID();
      const answer: IAnswerDocument = new Answer();
      answer.body = "Answer nr one";
      answer.question_id = questionId;
      answer.save().then(res => {
          expect(res).to.be.an("object");
          expect(res.body).to.be.equal("Answer nr one");
          expect(res.question_id).to.be.equal(questionId);
          done();
      }).catch(e => done(e));
    });
});
