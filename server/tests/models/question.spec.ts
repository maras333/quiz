import { Question, IQuestionDocument, Answer } from "../../models/models";
import { populateQuestions, questions, quizes } from "../seed/seed";
import * as chai from "chai";
import {ObjectID} from 'mongodb';


const expect = chai.expect;

beforeEach(populateQuestions);

describe("Model Question", () => {

  it("should create new question", (done: Function) => {
    const quizId = new ObjectID();
    const question: IQuestionDocument = new Question();
    question.body = "John";
    question.quiz_id = quizId;
    question.save().then(res => {
        expect(res).to.be.an("object");
        expect(res.body).to.be.equal("John");
        expect(res.quiz_id).to.be.equal(quizId);
        done();
    }).catch(e => done(e));

  });

  it("should find questions by quiz topic", (done: Function) => {
    const topic = "Astronomy";
    Question.findByQuizTopic(topic).then(questions => {
        expect(questions).to.have.lengthOf(3);
        done();
    }).catch(e => done(e));
  });

  it("should create question to existing quiz", (done: Function) => {
      const quizId = quizes[3]._id.toHexString();
      const questionBody = { body: "New question?" };
      Question.createQuestionToQuiz(questionBody, quizId).then(question => {
          expect(question.body).to.be.equal("New question?");
          expect(question.quiz_id.toHexString()).to.equals(quizId);
          done();
      }).catch(e => done(e));
  });

  it("should delete question with existing answers", (done: Function) => {
      const questionId = questions[0]._id.toHexString();
      Question.deleteWithAnswers(questionId).then(res => {
          Question.find({_id: questionId}).then(questions => {
              expect(questions).to.have.lengthOf(0);
          }).catch(e => done(e));
          Answer.find({questionId: questionId}).then(answers => {
              expect(answers).to.have.lengthOf(0);
          }).catch(e => done(e));
          done();
      })
   });

   it("should return TRUE if topic has it's own questions", (done: Function) => {
       const topic = "Astronomy";
       Question.checkQuestionsExistInTopic(topic).then(res => {
           expect(res).to.be.equal(true);
           done();
       }).catch(e => done(e));
    });

   it("should return FALSE if topic has NOT it's own questions", (done: Function) => {
       const topic = "Geography";
       Question.checkQuestionsExistInTopic(topic).then(res => {
           expect(res).to.be.equal(false);
           done();
       }).catch(e => done(e));
    });
});
