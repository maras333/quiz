import * as chai from "chai";

import { server } from "../../app";
import chaiHttp = require("chai-http");
import { Answer } from "../../models/models";
import { populateQuestions, questions, populateAnswers, answers } from "../seed/seed";


const expect = chai.expect;
chai.use(chaiHttp);

beforeEach(populateQuestions);
beforeEach(populateAnswers);

describe("Api Answer", function (): void {

    it("should add answer to specyfic question", (done: Function): void => {
    const questionId = questions[0]._id.toHexString();

    chai.request(server)
      .post(`/api/answer/${questionId}`)
      .set("content-type", "application/json")
      .send({
        body: "Answer nr 4",
        isTrue: true
      })
      .end((err: Error, res: any): void => {
        if(err) {
          return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.body).to.be.equal("Answer nr 4");
        expect(res.body.isTrue).to.be.equal(true);
        Answer.find({question_id: questionId}).then((questions) => {
            expect(questions).to.have.lengthOf(4);
        }).catch(e => done(e));        
        done();
      });
    });
});
