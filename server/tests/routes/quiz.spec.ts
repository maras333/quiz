import * as chai from "chai";

import { server } from "../../app";
import chaiHttp = require("chai-http");
import { Quiz, IQuizDocument } from "../../models/models";
import { populateQuizes, quizes } from "../seed/seed";


const expect = chai.expect;
chai.use(chaiHttp);

beforeEach(populateQuizes);

describe("Api Quiz", function (): void {

    it("should be able to get all quizes", (done: Function): void => {
      chai.request(server)
        .get("/api/quiz")
        .set("content-type", "application/json")
        .end((err: Error, res: any): void => {
          expect(res.statusCode).to.be.equal(200);
          expect(res.body).to.have.lengthOf(4);
          done();
        });
    });

    it("should be able to create quiz", (done: Function): void => {
    chai.request(server)
      .post("/api/quiz")
      .set("content-type", "application/json")
      .send({
        topic: "chemistry",
        name: "New chemistry quiz"
      })
      .end((err: Error, res: any): void => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.topic).to.be.equal("chemistry");
        expect(res.body.name).to.be.equal("New chemistry quiz");
        done();
      });
    });
});
