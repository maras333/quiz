import * as chai from "chai";

import { server } from "../../app";
import chaiHttp = require("chai-http");
import { Question, Answer } from "../../models/models";
import { populateQuestions, questions, populateQuizes, quizes } from "../seed/seed";


const expect = chai.expect;
chai.use(chaiHttp);

beforeEach(populateQuestions);
beforeEach(populateQuizes);

describe("Api Question", function (): void {

    it("should be able to get all questions", (done: Function): void => {
      chai.request(server)
        .get("/api/question")
        .set("content-type", "application/json")
        .end((err: Error, res: any): void => {
          expect(res.statusCode).to.be.equal(200);
          expect(res.body).to.have.lengthOf(4);
          done();
        });
    });

    it("should be able to get question with it's answers", (done: Function): void => {
    const questionId = questions[0]._id.toHexString();

    chai.request(server)
      .get(`/api/questionDetails/${questionId}`)
      .set("content-type", "application/json")
      .end((err: Error, res: any): void => {
        if(err) {
            return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        expect(res.body._id).to.be.equal(questionId);
        expect(res.body.answers).to.have.lengthOf(3);
        done();
      });
    });

    it("should be able to get question according to chosen topic", (done: Function): void => {
    const topic = 'Astronomy';

    chai.request(server)
      .get(`/api/questionByTopic/${topic}`)
      .set("content-type", "application/json")
      .end((err: Error, res: any): void => {
        if(err) {
            return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        expect(res.body).to.have.lengthOf(3);
        done();
      });
    });

    it("should be able to create question to specified quiz", (done: Function): void => {
    const quizId = quizes[3]._id.toHexString();
    const newQuestion = "Is this a Sun?";
    chai.request(server)
      .post(`/api/question/${quizId}`)
      .set("content-type", "application/json")
      .send({
          body: newQuestion
      })
      .end((err: Error, res: any): void => {
        if(err) {
          return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.body).to.be.equal(newQuestion);
        Question.find().then((questions) => {
            expect(questions).to.have.lengthOf(5);
            done();
        }).catch(e => done(e));
      });
    });

    it("should be able to delete question with it's answers", (done: Function): void => {
    const questionId = questions[0]._id.toHexString();
    chai.request(server)
      .del(`/api/question/${questionId}`)
      .set("content-type", "application/json")
      .end((err: Error, res: any): void => {
        if(err) {
          return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        Question.find({_id: questionId}).then((questions) => {
            expect(questions).to.have.lengthOf(0);
        }).catch(e => done(e));
        Answer.find({question_id: questionId}).then((answers) => {
            expect(answers).to.have.lengthOf(0);
        }).catch(e => done(e));
        done();
      });
    });

    it("should give POSITIVE info if given topic has questions", (done: Function): void => {
    const topic = 'Astronomy';
    chai.request(server)
      .get(`/api/checkQuestionsExist/${topic}`)
      .set("content-type", "application/json")
      .end((err: Error, res: any): void => {
        if(err) {
          return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.message).to.be.equal(`Questions for topic ${topic} exists`);
        done();
      });
    });

    it("should give NEGATIVE info if given topic has questions", (done: Function): void => {
    const topic = 'Random topic';
    chai.request(server)
      .get(`/api/checkQuestionsExist/${topic}`)
      .set("content-type", "application/json")
      .end((err: Error, res: any): void => {
        if(err) {
          return done(err);
        }
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.message).to.be.equal(`Questions for topic ${topic} NOT exists`);
        done();
      });
    });

});
