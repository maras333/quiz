import { Request, Response } from "express";
import { Quiz, IQuizDocument } from "../models/quiz/model";
import { Question, IQuestionDocument } from "../models/question/model";

export default {
    getAllQuestions: async (request: Request, response: Response) => {
        try {
            const questions: IQuestionDocument[] = await Question.find({}).exec();
            response.json(questions)
        } catch (e) {
            response.status(400).json(e)
        }
    },

    getQuestionDetails: async (request: Request, response: Response) => {
        try {
            const question: IQuestionDocument = await Question.findById(request.params.id).populate('answers').exec();
            if(!question) return response.json({msg: `Question with id ${request.params.id} not found`})
            response.json(question)
        } catch (e) {
            response.status(400).json(e)
        }
    },

    getQuestionByTopic: async (request: Request, response: Response) => {
        try {
            const questions: IQuestionDocument[] = await Question.findByQuizTopic(request.params.topic);
            response.json(questions)
        } catch (e) {
            response.status(400).json({ msg: e })
        }
    },

    createQuestionToQuiz: async (request: Request, response: Response) => {
        try {
            const question: IQuestionDocument = await Question.createQuestionToQuiz(request.body, request.params.quizId);
            response.json(question)
        } catch (e) {
            response.status(400).json(e);
        }
    },

    deleteQuestionWithAnswers: async (request: Request, response: Response) => {
        try {
          const result = await Question.deleteWithAnswers(request.params.id);
          if(result.status === 400) {
            response.status(400).json({ msg: result.msg })
            return;
          } else if (result.status === 404) {
            response.status(404).json({ msg: result.msg })
            return;
          }
          response.status(200).json({ msg: result.msg });
        } catch (e) {
          response.status(400).json({ errorMsg: e });
        }
    },

    checkQuestionsExist: async (request: Request, response: Response) => {
        try {
          const exists = await Question.checkQuestionsExistInTopic(request.params.topic);
          if(exists) {
            response.status(200).json({ message: `Questions for topic ${request.params.topic} exists` })
          } else {
            response.status(200).json({ message: `Questions for topic ${request.params.topic} NOT exists` });
          }
        } catch (e) {
          response.status(400).json({ errorMsg: e });
        }
    }



}
