import { Request, Response } from "express";
import { Answer, IAnswerDocument } from "../models/answer/model";
import { Question, IQuestionDocument } from "../models/question/model";

export default {
    getAllAnswers: async (request: Request, response: Response) => {
      try {
        const answers = await Answer.find({}).exec();
        response.status(200).json(answers);
      } catch (e) {
        response.status(400).json({ errorMsg: e });
      }
    },

    createAnswerToQuestion: async (request: Request, response: Response) => {
      try {
        let answerData: IAnswerDocument = request.body;
        const question: IQuestionDocument = await Question.findById(request.params.questionId).exec();
        if(!question) {
          response.status(404).json({msg: `Question with id ${request.params.questionId} not found`});
          return;
        }
        answerData.question_id = question._id;
        const answer: IAnswerDocument = await Answer.create(answerData);
        await Question.update(
          { _id: question._id },
          { $push: { answers: answer._id } }
        );
        response.status(200).json(answer);
      } catch (e) {
        response.status(400).json({ errorMsg: e });
      }
    }
}
