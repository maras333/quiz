import { Request, Response } from "express";
import { Quiz } from "../models/quiz/model";

export default {
    getAllQuizes: async (request: Request, response: Response) => {
        try {
          const quizes = await Quiz.find({}).exec();
          response.json(quizes)
        } catch (e) {
          response.status(400).json({ errorMsg: e });
        }
    },

    createQuiz: async (request: Request, response: Response) => {
        try {
          request.body.topic = request.body.topic.toLowerCase();
          const quiz = await Quiz.create(request.body);
          if(!quiz) {
            response.status(400).json({msg: `Quiz not created`});
            return;
          }
          response.status(200).json(quiz);
        } catch (e) {
          response.status(400).json({ errorMsg: e });
        }
    }
}
