import * as mongoose from "mongoose";

(mongoose as any).Promise = global.Promise;

let connection: mongoose.Connection = mongoose.createConnection(process.env.MONGODB_URI);

export { connection };
