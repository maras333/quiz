import { connection } from "../../config/database";
import { Document, Model, Schema } from "mongoose";
import { Quiz, IQuizDocument, IQuizModel } from "../quiz/model";
import { Answer, AnswerSchema, IAnswerDocument } from "../answer/model";

export interface IQuestionDocument extends Document {
  body: string;
  create: Date;
  answers: [],
  quiz_id: {}
}

export interface IQuestionModel extends Model<IQuestionDocument> {
  findByQuizTopic(topic: string): Promise<IQuestionDocument[]>
  deleteWithAnswers(id: string): Promise<{ success: Boolean, status: Number, msg: String }>
  checkQuestionsExistInTopic(topic: string): Promise<Boolean>
  createQuestionToQuiz(body, quizId: string)
}

const QuestionSchema = new Schema({
  body: {
    type: String,
    required: true
  },
  create: {
    type: Date,
    default: Date.now
  },
  answers: [{
      type: Schema.Types.ObjectId,
      ref: 'Answer'
  }],
  quiz_id: {
    type: Schema.Types.ObjectId,
    ref: 'Quiz',
    required: true
  },
});

QuestionSchema.static("findByQuizTopic", async (topic: string) => {
    try {
      let topicIds: number[] = await Quiz.find({ topic: topic.toLowerCase() }).distinct('_id');
      return Question
      .find({ quiz_id: { $in: topicIds }})
      .lean()
      .exec();
    } catch (e) {
      return { e }
    }
});

QuestionSchema.static("createQuestionToQuiz", async (questionBody, quizId: string) => {
    try {
        const quiz: IQuizDocument = await Quiz.findOne({ _id: quizId }).exec();
        if(!quiz) {
            return { success: false, status: 404, msg: `Quiz with id: ${quizId} NOT exists. Question not created` }
        }
        questionBody.quiz_id = quiz._id;
        const question: IQuestionDocument | {} = await Question.create(questionBody).catch(e => { return { errorMsg: e.message} });
        return question;
    } catch (e) {
        return { success: false, status: 400, msg: `Question ${quizId} not created` }
    }
});

QuestionSchema.static("deleteWithAnswers", async (id: string) => {
    try {
        let removedQuestion: IQuestionDocument = await Question.findOneAndRemove({ _id: id }).exec();
        if(!removedQuestion) {
            return Promise.resolve({ success: false, status: 404, msg: `Question ${id} NOT exists` })
        }
        await Answer.deleteMany({ question_id: id});
        return Promise.resolve({ success: true, status: 200, msg: `Question ${id} removed` })
    } catch (e) {
        return Promise.resolve({ success: true, status: 400, msg: `Question ${id} not removed` })
    }
});

QuestionSchema.static("checkQuestionsExistInTopic", async (topic: string) => {
    try {
        let topicIds: number[] = await Quiz.find({ topic: topic.toLowerCase() }).distinct('_id');
        let questionIds: IQuestionDocument[] = await Question.find({ quiz_id: { $in: topicIds } }).select('_id');
        if(questionIds.length) return Promise.resolve(true);
        else return Promise.resolve(false);
    } catch (e) {
        return Promise.reject(e);
    }
});

export const Question: IQuestionModel = connection.model<IQuestionDocument, IQuestionModel>("Question", QuestionSchema);
