import { connection } from "../../config/database";
import { Document, Model, Schema } from "mongoose";

export interface IQuizDocument extends Document {
  topic: string;
  name: string;
  create?: Date;
}

export interface IQuizModel extends Model<IQuizDocument> {
    // posible to declare static methods here
}

const QuizSchema = new Schema({
  topic: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: [true, 'Type name of quiz!']
  },
  create: {
    type: Date,
    "default": Date.now
  }
});

export const Quiz: IQuizModel = connection.model<IQuizDocument, IQuizModel>("Quiz", QuizSchema);
