import { connection } from "../../config/database";
import { Document, Model, Schema } from "mongoose";

export interface IAnswerDocument extends Document {
  body: string;
  create: Date;
  isTrue: Boolean;
  question_id: {}
}

export interface IAnswerModel extends Model<IAnswerDocument> {
    // posible to declare static methods here
}

export const AnswerSchema = new Schema({
  body: {
    type: String,
    required: true
  },
  create: {
    type: Date,
    "default": Date.now
  },
  isTrue: {
     type: Boolean,
     default: false
  },
  question_id: {
    type: Schema.Types.ObjectId,
    ref: 'Question',
    required: true
  },
});

export const Answer: IAnswerModel = connection.model<IAnswerDocument, IAnswerModel>("Answer", AnswerSchema);
