import { Router } from "express";
import QuestionController from "../../controllers/question";

export class QuestionRouter {

  static routes(): Router {

    return Router()

      .get("/question", QuestionController.getAllQuestions)

      .get("/questionDetails/:id", QuestionController.getQuestionDetails)

      .get("/questionByTopic/:topic", QuestionController.getQuestionByTopic)

      .post("/question/:quizId", QuestionController.createQuestionToQuiz)

      .delete("/question/:id", QuestionController.deleteQuestionWithAnswers)

      .get("/checkQuestionsExist/:topic", QuestionController.checkQuestionsExist)
  }
}
