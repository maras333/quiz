import { Router, Request, Response } from "express";
import QuizController from '../../controllers/quiz';

export class QuizRouter {

    static routes(): Router {
        return Router()

        .get("/quiz", QuizController.getAllQuizes)

        .post("/quiz", QuizController.createQuiz);
    }
}
