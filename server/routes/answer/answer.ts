import { Request, Response, Router } from "express";
import { Quiz } from "../../models/quiz/model";
import AnswerController from "../../controllers/answer";

export class AnswerRouter {

  static routes(): Router {

    return Router()
      // get all answers
      .get("/answer", AnswerController.getAllAnswers)
      // add answer to a specific question
      .post("/answer/:questionId", AnswerController.createAnswerToQuestion);
  }
}
