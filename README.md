# Quiz manager app

- tested on `node v8.15.0` and `MongoDB v4.0.2`
## Mongoose & Typescript & Express

- Unit tests with Mocha and Chai

## Database setup

- Create catalog to store databases e.g: mongo-data
- Go to directory with installed MongoDB
- Run: e.g

``` bash
cd mongo/bin
./mongod --dbpath ~/mongo-data
```
`--dbpath` is a path to place where mongo databases are stored

## Installation

```bash
npm install

npm start
```

Tests
```bash
npm test
```
Tests are using it's own database

## URL

* API endpoint: http://localhost:3000

## API description
Quiz  
`GET /api/quiz` - List of all quizes registered in system.  
`POST /api/quiz` - Create new quiz. (Required req.body: {name, topic})  
Question  
`GET /api/question` - List all questions in system.  
`GET /api/questionDetails/:id` - List given question with it's answers.  
`GET /api/questionByTopic/:topic` - List all questions added to quizes with given topic (one or more quizes can have the same. topic)  
`POST /api/question/:quizId` - Create question for specified quizId. (Required req.body: {body})  
`DELETE /api/question/:id` - Delete qiven question with it's answers.  
`GET /api/checkQuestionsExist/:topic` - Check if given topic have it's own questions  
Answer  
`GET /api/answer` - List of all answers registered in system.  
`POST /api/answer/:questionId` - Create answer for specified question (Required req.body: {body})  
